import java.sql.*;

public class State {

    private int id = -1;
    private String name;

    public State(String name) {
        this.name = name;
    }

    public State() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "State{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean newState(){
        return id >= 0 ? false : true;
    }

    public boolean saveToDatabase() throws SQLException {
        boolean success = false;

        Connection connection = JdbcBasics.getDataSource().getConnection();
        if(id < 0){
            String query = String.format("insert into states(name) values('%s')", this.name);
            PreparedStatement statement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if(rs.next()) {
                id = rs.getInt("id");
            }
        } else {
            String query = String.format("update states set name='%s' where id = %d", this.name, this.id);
            Statement statement = connection.createStatement();
            statement.execute(query);
        }
        connection.close();
        return true;
    }


}
