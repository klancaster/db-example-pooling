import java.sql.*;

public class StateManager {

    public static void saveState(State state) throws SQLException {
        Connection connection = JdbcBasics.getDataSource().getConnection();
        String insertQuery = String.format("insert into states(name) values('%s')", state.getName());
        String updateQuery = String.format("update states set name='%s' where id = %d ", state.getName(),state.getId());

        // CHALLENGE: Can we make this more efficient (less code duplication)?
        if(state.newState()){
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if(rs.next()){
                state.setId(rs.getInt("id"));
            }
            preparedStatement.close();
            connection.close();
        } else {
            Statement statement = connection.createStatement();
            statement.execute(updateQuery);
            statement.close();
            connection.close();
        }
    }

    public static State loadState(int stateId) throws SQLException {
        State state = new State();
        Connection connection = JdbcBasics.getDataSource().getConnection();
        Statement statement = connection.createStatement();

        return state;
    }


}
