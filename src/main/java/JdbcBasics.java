import org.apache.commons.dbcp.BasicDataSource;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

/**
 * Simple class to access a database using JDBC
 */

public class JdbcBasics {
    private static final String INSERT_STATE = "INSERT INTO states(name) VALUES(?)";
//    private static final String DB_URL = "jdbc:postgresql://localhost:5432/jdbc-basics";
    private static final String DB_URL = "jdbc:h2:./target/db";
    private static final String password = "1234";
    private static final String username = "sa";
//    private static final String password = "postgres";
//    private static final String username = "postgres";
    private static BasicDataSource dataSource;

    public static BasicDataSource getDataSource() {
        return dataSource;
    }

    public static void main(String[] args) throws SQLException {

        initializeDataSource();

        System.out.println("JDBC Basics");

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STATE);
        List<String> names = Arrays.asList("Texas","Oklahoma","Washington","Kansas");
        for(String name : names){
            preparedStatement.setString(1, name);
            preparedStatement.execute();
        }
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM states");
        while (resultSet.next()) {
            System.out.println(resultSet.getString("name"));
        }
        connection.close();

        State state = new State();
        state.setName("New York");
        state.saveToDatabase();
        System.out.println(state);

        // now modify the state
        state.setName("Harry Potter");
        state.saveToDatabase();

        // now use the StateManager
        State newState = new State("Florida");
        StateManager.saveState(newState);
        System.out.println(newState);

        // CHALLENGE: Why does newState contain the ID
        // HINT: Read https://stackoverflow.com/questions/7893492/is-java-really-passing-objects-by-value#7893495

    }

    private static void initializeDataSource() {
        dataSource = new BasicDataSource();
        dataSource.setUrl(DB_URL);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
    }
}
